# 0.4.0

* [#20](https://gitlab.com/bowlofeggs/rpick/merge_requests/20): Add an LRU model.


# 0.3.1

* [#19](https://gitlab.com/bowlofeggs/rpick/merge_requests/19): Adjust the tests to pass on 32-bit
  architectures.


# 0.3.0

* [#5](https://gitlab.com/bowlofeggs/rpick/merge_requests/5): Introduced a unit test suite.
* [#8](https://gitlab.com/bowlofeggs/rpick/merge_requests/8): Defined a library for rpick so
  integrators can write their own front end to it. This also aided in testing.
* [#11](https://gitlab.com/bowlofeggs/rpick/merge_requests/11): Added documentation for the library.
* [#14](https://gitlab.com/bowlofeggs/rpick/merge_requests/14): Users will no longer be re-prompted
  for a choice they've declined in the same process, unless they decline all possible choices in a
  category.
* [d20e491b](https://gitlab.com/bowlofeggs/rpick/commit/d20e491b5971b73dd27d46bae3938f9321272517):
  Documented installation.


# 0.2.0

* [#3](https://gitlab.com/bowlofeggs/rpick/merge_requests/3): Added a new ```even``` distribution
  model, which does a nice flat random pick.
* [#4](https://gitlab.com/bowlofeggs/rpick/merge_requests/4): Added a new ```weighted```
  distribution model, which does a weighted random pick.
* [95b32b1e](https://gitlab.com/bowlofeggs/rpick/commit/95b32b1e4c103843cf3af900d94f5fef3ca286df):
  Added a new ```lottery``` distribution model, which gives lottery tickets to unpicked items and
  resets the picked item's lottery tickets to 0.


# 0.1.0

* [#1](https://gitlab.com/bowlofeggs/rpick/merge_requests/1): Added a new
  ```stddev_scaling_factor``` setting, which is optional and defaults to ```3.0```.
* [#2](https://gitlab.com/bowlofeggs/rpick/merge_requests/2): The model now defaults to "gaussian",
  so users don't have to define it by hand.
